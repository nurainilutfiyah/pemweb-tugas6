<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShowController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/mahasiswa', [ShowController::class, 'index']);
// Route::get('/mhs/create', [ShowController::class, 'create'])->name('mhs.create');
// Route::post('/mhs/store', [ShowController::class, 'store'])->name('mhs.store');

Route::resource('/mhs', ShowController::class);