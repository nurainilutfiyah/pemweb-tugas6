<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;

class ShowController extends Controller
{
    public function index()
    {
       
        $mhs = Mahasiswa::all();

        return view('mhs.index', compact('mhs'));
    }

    public function create()
    {
        return view('mhs.create');
    }

    
    public function store(Request $request)
    {
        //validate form
        $this->validate($request, [
            'nim'     => 'required',
            'nama'   => 'required'
        ]);

       
        Mahasiswa::create([
            'nim'     => $request->nim,
            'nama'   => $request->nama
        ]);

        //redirect to index
        return redirect()->route('mhs.index');
    }
}
